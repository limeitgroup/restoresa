﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoResa.Domain
{
    [Table("Commande")]
    public class Commande
    {
        public Commande()
        {
            LigneCommandes = new HashSet<LigneCommande>();
        }

        [Key]
        public int ID { get; set; }

        public int NbPlaces { get; set; }

        public DateTime DateCommande { get; set; }

        public int UtilisateurID { get; set; }

        public int StatutCommandeID { get; set; }

        public int ServiceHoraireID { get; set; }

        public virtual Utilisateur Utilisateur { get; set; }

        public virtual StatutCommande StatutCommande { get; set; }

        public virtual ServiceHoraire ServiceHoraire { get; set; }

        public virtual ICollection<LigneCommande> LigneCommandes { get; set; }


    }
}
