﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoResa.Domain
{
    public class TJMenuServiceHoraire
    {
        [Key, Column(Order = 0)]
        public int MenuID { get; set; }

        [Key, Column(Order = 1)]
        public int ServiceHoraireID { get; set; }

        public virtual Menu Menu { get; set; }

        public virtual ServiceHoraire ServiceHoraire { get; set; }
    }
}
