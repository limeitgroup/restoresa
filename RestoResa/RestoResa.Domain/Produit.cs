﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoResa.Domain
{
    [Table("Produit")]
    public class Produit
    {
        public Produit()
        {
            TJMenuProduit = new HashSet<TJMenuProduit>();
        }

        public int ID { get; set; }

        public string NomProduit { get; set; }

        public string DescriptionProduit { get; set; }

        public virtual ICollection<TJMenuProduit> TJMenuProduit { get; set; }
    }
}
