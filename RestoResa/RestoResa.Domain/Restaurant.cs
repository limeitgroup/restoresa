﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoResa.Domain
{
    [Table("Restaurant")]
    public class Restaurant
    {
        public Restaurant()
        {
            Personnels = new HashSet<Utilisateur>();
            ServiceHoraires = new HashSet<ServiceHoraire>();
            TJRestaurantMoyenPaiement = new HashSet<TJRestaurantMoyenPaiement>();
            TJRestaurantSpecialite = new HashSet<TJRestaurantSpecialite>();
        }

        [Key]
        public int ID { get; set; }

        public string RaisonSociale { get; set; }

        public string Description { get; set; }

        public string Adresse { get; set; }

        public string Ville { get; set; }

        public string CodePostal { get; set; }

        public string Pays { get; set; }

        public string Mail { get; set; }

        public string Telephone { get; set; }



        public virtual ICollection<Utilisateur> Personnels { get; set; }

        public virtual ICollection<ServiceHoraire> ServiceHoraires { get; set; }
        public virtual ICollection<TJRestaurantMoyenPaiement> TJRestaurantMoyenPaiement { get; set; }

        public virtual ICollection<TJRestaurantSpecialite> TJRestaurantSpecialite { get; set; }


    }
}
