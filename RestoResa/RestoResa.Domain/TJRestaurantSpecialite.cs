﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoResa.Domain
{
    public class TJRestaurantSpecialite
    {
        [Key, Column(Order = 0)]
        public int RestaurantID { get; set; }

        [Key, Column(Order = 1)]
        public int SpecialiteID { get; set; }

        public Restaurant Restaurant { get; set; }

        public Specialite Specialite { get; set; }

    }
}
