﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoResa.Domain
{
    [Table("Utilisateur")]
    public class Utilisateur
    {
        public Utilisateur()
        {
            Commandes = new HashSet<Commande>();
        }

        [Key]
        public long ID { get; set; }

        public string Nom { get; set; }

        public string Prenom { get; set; }

        public string Mail { get; set; }

        public string Password { get; set; }

        public int ProfilID { get; set; }

        public int? RestaurantID { get; set; }

        public virtual Profil Profil { get; set; }

        public virtual Restaurant Restaurant { get; set; }

        public virtual ICollection<Commande> Commandes { get; set; }
    }
}
