﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoResa.Domain
{
    [Table("Menu")]
    public class Menu
    {
        public Menu()
        {
            TJMenuServiceHoraire = new HashSet<TJMenuServiceHoraire>();
            TJMenuProduit = new HashSet<TJMenuProduit>();
        }

        [Key]
        public int ID { get; set; }

        public string NomMenu { get; set; }

        public string DescriptionMenu { get; set; }

        public decimal PrixMenu { get; set; }

        public virtual ICollection<TJMenuServiceHoraire> TJMenuServiceHoraire { get; set; }

        public virtual ICollection<TJMenuProduit> TJMenuProduit { get; set; }
    }
}
