﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoResa.Domain
{
    [Table("LigneCommande")]
    public class LigneCommande
    {
        [Key]
        public int ID { get; set; }

        public int CommandeID { get; set; }

        public int MenuID { get; set; }

        public virtual Commande Commande { get; set;  }

        public virtual Menu Menu { get; set; }
    }
}
