﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoResa.Domain
{
    [Table("Profil")]
    public class Profil
    {
        public Profil()
        {
            Utilisateurs = new HashSet<Utilisateur>();
        }

        [Key]
        public int ID { get; set; }

        public string Code { get; set; }

        public string NomProfil { get; set; }

        public virtual ICollection<Utilisateur> Utilisateurs { get; set; }
    }
}
