﻿using RestoResa.DataAccessLayer;
using System;
using System.Web.Mvc;

namespace RestoResa.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

    }
}
