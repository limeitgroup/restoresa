﻿(function () {
    angular.module("restoResa", [])
    angular.module('restoResa')
        .controller("Home", ['$scope', HomeControllerFactory])

    function HomeControllerFactory($scope) {
        $scope.data = "Bonjour !";
    }
})()