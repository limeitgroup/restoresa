﻿using System.Web.Optimization;

namespace RestoResa
{
    public class BundleConfig
    {
        // Pour plus d'informations sur Bundling, accédez à l'adresse http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            "~/Content/js/jquery.min.js",
            "~/Content/js/jquery.easing.1.3.js",
            "~/Content/js/jquery.waypoints.min.js",
            "~/Content/js/jquery.countTo.js",
            "~/Content/js/jquery.stellar.min.js",
            "~/Content/js/jquery.magnific-popup.min.js"
            ));

            bundles.Add(new ScriptBundle("~/Content/owljs").Include(
               "~/Content/js/bootstrap.min.js",
               "~/Content/js/owl.carousel.min.js",
               "~/Content/js/magnific-popup-options.js",
               "~/Content/js/moment.min.js",
               "~/Content/js/bootstrap-datetimepicker.min.js",
               "~/Content/js/main.js"
               ));

            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
            //            "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Utilisez la version de développement de Modernizr pour développer et apprendre. Puis, lorsque vous êtes
            // prêt pour la production, utilisez l'outil de génération sur http://modernizr.com pour sélectionner uniquement les tests dont vous avez besoin.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));


            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/css/animate.css",
                "~/Content/css/icomoon.css",
                "~/Content/css/themify-icons.css",
                "~/Content/css/magnific-popup.css",
                "~/Content/css/style.css"
                ));

            bundles.Add(new StyleBundle("~/Content/bootstrapcss").Include(
                "~/Content/css/bootstrap.css",
                "~/Content/css//bootstrap-datetimepicker.min.css"
                ));

            bundles.Add(new StyleBundle("~/Content/owlcss").Include(
                "~/Content/css/owl.carousel.min.css",
                "~/Content/css/owl.theme.default.min.css"
                ));

            //bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            //bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
            //            "~/Content/themes/base/jquery.ui.core.css",
            //            "~/Content/themes/base/jquery.ui.resizable.css",
            //            "~/Content/themes/base/jquery.ui.selectable.css",
            //            "~/Content/themes/base/jquery.ui.accordion.css",
            //            "~/Content/themes/base/jquery.ui.autocomplete.css",
            //            "~/Content/themes/base/jquery.ui.button.css",
            //            "~/Content/themes/base/jquery.ui.dialog.css",
            //            "~/Content/themes/base/jquery.ui.slider.css",
            //            "~/Content/themes/base/jquery.ui.tabs.css",
            //            "~/Content/themes/base/jquery.ui.datepicker.css",
            //            "~/Content/themes/base/jquery.ui.progressbar.css",
            //            "~/Content/themes/base/jquery.ui.theme.css"));

            //Angular
            bundles.Add(new ScriptBundle("~/Angular").Include("~/Scripts/angular.js"));
            bundles.Add(new ScriptBundle("~/AngularContent").Include("~/Angular/Modules.js"));
            //
        }
    }
}