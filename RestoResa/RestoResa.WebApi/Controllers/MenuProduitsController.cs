﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using RestoResa.Model;
using RestoResa.WebApi.Model;

namespace RestoResa.WebApi.Controllers
{
    public class MenuProduitsController : ApiController
    {
        private RestoResaWebApiContext db = new RestoResaWebApiContext();

        // GET: api/MenuProduits
        public IQueryable<MenuProduit> GetMenuProduits()
        {
            return db.MenuProduits;
        }

        // GET: api/MenuProduits/5
        [ResponseType(typeof(MenuProduit))]
        public async Task<IHttpActionResult> GetMenuProduit(int id)
        {
            MenuProduit MenuProduit = await db.MenuProduits.FindAsync(id);
            if (MenuProduit == null)
            {
                return NotFound();
            }

            return Ok(MenuProduit);
        }

        // PUT: api/MenuProduits/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutMenuProduit(int id, MenuProduit MenuProduit)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != MenuProduit.MenuID)
            {
                return BadRequest();
            }

            db.Entry(MenuProduit).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MenuProduitExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MenuProduits
        [ResponseType(typeof(MenuProduit))]
        public async Task<IHttpActionResult> PostMenuProduit(MenuProduit MenuProduit)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MenuProduits.Add(MenuProduit);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (MenuProduitExists(MenuProduit.MenuID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = MenuProduit.MenuID }, MenuProduit);
        }

        // DELETE: api/MenuProduits/5
        [ResponseType(typeof(MenuProduit))]
        public async Task<IHttpActionResult> DeleteMenuProduit(int id)
        {
            MenuProduit MenuProduit = await db.MenuProduits.FindAsync(id);
            if (MenuProduit == null)
            {
                return NotFound();
            }

            db.MenuProduits.Remove(MenuProduit);
            await db.SaveChangesAsync();

            return Ok(MenuProduit);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MenuProduitExists(int id)
        {
            return db.MenuProduits.Count(e => e.MenuID == id) > 0;
        }
    }
}