﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using RestoResa.Model;
using RestoResa.WebApi.Model;

namespace RestoResa.WebApi.Controllers
{
    public class RestaurantSpecialitesController : ApiController
    {
        private RestoResaWebApiContext db = new RestoResaWebApiContext();

        // GET: api/RestaurantSpecialites
        public IQueryable<RestaurantSpecialite> GetRestaurantSpecialites()
        {
            return db.RestaurantSpecialites;
        }

        // GET: api/RestaurantSpecialites/5
        [ResponseType(typeof(RestaurantSpecialite))]
        public async Task<IHttpActionResult> GetRestaurantSpecialite(int id)
        {
            RestaurantSpecialite RestaurantSpecialite = await db.RestaurantSpecialites.FindAsync(id);
            if (RestaurantSpecialite == null)
            {
                return NotFound();
            }

            return Ok(RestaurantSpecialite);
        }

        // PUT: api/RestaurantSpecialites/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutRestaurantSpecialite(int id, RestaurantSpecialite RestaurantSpecialite)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != RestaurantSpecialite.RestaurantID)
            {
                return BadRequest();
            }

            db.Entry(RestaurantSpecialite).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RestaurantSpecialiteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/RestaurantSpecialites
        [ResponseType(typeof(RestaurantSpecialite))]
        public async Task<IHttpActionResult> PostRestaurantSpecialite(RestaurantSpecialite RestaurantSpecialite)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.RestaurantSpecialites.Add(RestaurantSpecialite);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (RestaurantSpecialiteExists(RestaurantSpecialite.RestaurantID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = RestaurantSpecialite.RestaurantID }, RestaurantSpecialite);
        }

        // DELETE: api/RestaurantSpecialites/5
        [ResponseType(typeof(RestaurantSpecialite))]
        public async Task<IHttpActionResult> DeleteRestaurantSpecialite(int id)
        {
            RestaurantSpecialite RestaurantSpecialite = await db.RestaurantSpecialites.FindAsync(id);
            if (RestaurantSpecialite == null)
            {
                return NotFound();
            }

            db.RestaurantSpecialites.Remove(RestaurantSpecialite);
            await db.SaveChangesAsync();

            return Ok(RestaurantSpecialite);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RestaurantSpecialiteExists(int id)
        {
            return db.RestaurantSpecialites.Count(e => e.RestaurantID == id) > 0;
        }
    }
}