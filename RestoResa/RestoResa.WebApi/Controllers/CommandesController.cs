﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using RestoResa.Model;
using RestoResa.WebApi.Model;

namespace RestoResa.WebApi.Controllers
{
    public class CommandesController : ApiController
    {
        private RestoResaWebApiContext db = new RestoResaWebApiContext();

        // GET: api/Commandes
        public IQueryable<Commande> GetCommandes()
        {
            return db.Commandes;
        }

        // GET: api/Commandes/5
        [ResponseType(typeof(Commande))]
        public async Task<IHttpActionResult> GetCommande(int id)
        {
            Commande commande = await db.Commandes.FindAsync(id);
            if (commande == null)
            {
                return NotFound();
            }

            return Ok(commande);
        }

        // PUT: api/Commandes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCommande(int id, Commande commande)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != commande.ID)
            {
                return BadRequest();
            }

            db.Entry(commande).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CommandeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Commandes
        [ResponseType(typeof(Commande))]
        public async Task<IHttpActionResult> PostCommande(Commande commande)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Commandes.Add(commande);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = commande.ID }, commande);
        }

        // DELETE: api/Commandes/5
        [ResponseType(typeof(Commande))]
        public async Task<IHttpActionResult> DeleteCommande(int id)
        {
            Commande commande = await db.Commandes.FindAsync(id);
            if (commande == null)
            {
                return NotFound();
            }

            db.Commandes.Remove(commande);
            await db.SaveChangesAsync();

            return Ok(commande);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CommandeExists(int id)
        {
            return db.Commandes.Count(e => e.ID == id) > 0;
        }
    }
}