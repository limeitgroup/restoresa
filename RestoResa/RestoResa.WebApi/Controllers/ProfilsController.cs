﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using RestoResa.Model;
using RestoResa.WebApi.Model;

namespace RestoResa.WebApi.Controllers
{
    public class ProfilsController : ApiController
    {
        private RestoResaWebApiContext db = new RestoResaWebApiContext();

        // GET: api/Profils
        public IQueryable<Profil> GetProfils()
        {
            return db.Profils;
        }

        // GET: api/Profils/5
        [ResponseType(typeof(Profil))]
        public async Task<IHttpActionResult> GetProfil(int id)
        {
            Profil profil = await db.Profils.FindAsync(id);
            if (profil == null)
            {
                return NotFound();
            }

            return Ok(profil);
        }

        // PUT: api/Profils/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutProfil(int id, Profil profil)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != profil.ID)
            {
                return BadRequest();
            }

            db.Entry(profil).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProfilExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Profils
        [ResponseType(typeof(Profil))]
        public async Task<IHttpActionResult> PostProfil(Profil profil)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Profils.Add(profil);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = profil.ID }, profil);
        }

        // DELETE: api/Profils/5
        [ResponseType(typeof(Profil))]
        public async Task<IHttpActionResult> DeleteProfil(int id)
        {
            Profil profil = await db.Profils.FindAsync(id);
            if (profil == null)
            {
                return NotFound();
            }

            db.Profils.Remove(profil);
            await db.SaveChangesAsync();

            return Ok(profil);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProfilExists(int id)
        {
            return db.Profils.Count(e => e.ID == id) > 0;
        }
    }
}