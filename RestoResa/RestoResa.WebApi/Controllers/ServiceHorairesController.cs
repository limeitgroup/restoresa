﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using RestoResa.Model;
using RestoResa.WebApi.Model;

namespace RestoResa.WebApi.Controllers
{
    public class ServiceHorairesController : ApiController
    {
        private RestoResaWebApiContext db = new RestoResaWebApiContext();

        // GET: api/ServiceHoraires
        public IQueryable<ServiceHoraire> GetServiceHoraires()
        {
            return db.ServiceHoraires;
        }

        // GET: api/ServiceHoraires/5
        [ResponseType(typeof(ServiceHoraire))]
        public async Task<IHttpActionResult> GetServiceHoraire(int id)
        {
            ServiceHoraire serviceHoraire = await db.ServiceHoraires.FindAsync(id);
            if (serviceHoraire == null)
            {
                return NotFound();
            }

            return Ok(serviceHoraire);
        }

        // PUT: api/ServiceHoraires/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutServiceHoraire(int id, ServiceHoraire serviceHoraire)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != serviceHoraire.ID)
            {
                return BadRequest();
            }

            db.Entry(serviceHoraire).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ServiceHoraireExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ServiceHoraires
        [ResponseType(typeof(ServiceHoraire))]
        public async Task<IHttpActionResult> PostServiceHoraire(ServiceHoraire serviceHoraire)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ServiceHoraires.Add(serviceHoraire);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = serviceHoraire.ID }, serviceHoraire);
        }

        // DELETE: api/ServiceHoraires/5
        [ResponseType(typeof(ServiceHoraire))]
        public async Task<IHttpActionResult> DeleteServiceHoraire(int id)
        {
            ServiceHoraire serviceHoraire = await db.ServiceHoraires.FindAsync(id);
            if (serviceHoraire == null)
            {
                return NotFound();
            }

            db.ServiceHoraires.Remove(serviceHoraire);
            await db.SaveChangesAsync();

            return Ok(serviceHoraire);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ServiceHoraireExists(int id)
        {
            return db.ServiceHoraires.Count(e => e.ID == id) > 0;
        }
    }
}