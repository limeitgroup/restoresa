﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using RestoResa.Model;
using RestoResa.WebApi.Model;

namespace RestoResa.WebApi.Controllers
{
    public class SpecialitesController : ApiController
    {
        private RestoResaWebApiContext db = new RestoResaWebApiContext();

        // GET: api/Specialites
        public IQueryable<Specialite> GetSpecialites()
        {
            return db.Specialites;
        }

        // GET: api/Specialites/5
        [ResponseType(typeof(Specialite))]
        public async Task<IHttpActionResult> GetSpecialite(int id)
        {
            Specialite specialite = await db.Specialites.FindAsync(id);
            if (specialite == null)
            {
                return NotFound();
            }

            return Ok(specialite);
        }

        // PUT: api/Specialites/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSpecialite(int id, Specialite specialite)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != specialite.ID)
            {
                return BadRequest();
            }

            db.Entry(specialite).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SpecialiteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Specialites
        [ResponseType(typeof(Specialite))]
        public async Task<IHttpActionResult> PostSpecialite(Specialite specialite)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Specialites.Add(specialite);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = specialite.ID }, specialite);
        }

        // DELETE: api/Specialites/5
        [ResponseType(typeof(Specialite))]
        public async Task<IHttpActionResult> DeleteSpecialite(int id)
        {
            Specialite specialite = await db.Specialites.FindAsync(id);
            if (specialite == null)
            {
                return NotFound();
            }

            db.Specialites.Remove(specialite);
            await db.SaveChangesAsync();

            return Ok(specialite);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SpecialiteExists(int id)
        {
            return db.Specialites.Count(e => e.ID == id) > 0;
        }
    }
}