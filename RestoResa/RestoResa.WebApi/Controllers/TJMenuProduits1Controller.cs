﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using RestoResa.Model;
using RestoResa.WebApi.Models;

namespace RestoResa.WebApi.Controllers
{
    public class TJMenuProduits1Controller : ApiController
    {
        private RestoResaWebApiContext db = new RestoResaWebApiContext();

        // GET: api/TJMenuProduits1
        public IQueryable<TJMenuProduit> GetTJMenuProduits()
        {
            return db.TJMenuProduits;
        }

        // GET: api/TJMenuProduits1/5
        [ResponseType(typeof(TJMenuProduit))]
        public async Task<IHttpActionResult> GetTJMenuProduit(int id)
        {
            TJMenuProduit tJMenuProduit = await db.TJMenuProduits.FindAsync(id);
            if (tJMenuProduit == null)
            {
                return NotFound();
            }

            return Ok(tJMenuProduit);
        }

        // PUT: api/TJMenuProduits1/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTJMenuProduit(int id, TJMenuProduit tJMenuProduit)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tJMenuProduit.MenuID)
            {
                return BadRequest();
            }

            db.Entry(tJMenuProduit).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TJMenuProduitExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TJMenuProduits1
        [ResponseType(typeof(TJMenuProduit))]
        public async Task<IHttpActionResult> PostTJMenuProduit(TJMenuProduit tJMenuProduit)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TJMenuProduits.Add(tJMenuProduit);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TJMenuProduitExists(tJMenuProduit.MenuID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = tJMenuProduit.MenuID }, tJMenuProduit);
        }

        // DELETE: api/TJMenuProduits1/5
        [ResponseType(typeof(TJMenuProduit))]
        public async Task<IHttpActionResult> DeleteTJMenuProduit(int id)
        {
            TJMenuProduit tJMenuProduit = await db.TJMenuProduits.FindAsync(id);
            if (tJMenuProduit == null)
            {
                return NotFound();
            }

            db.TJMenuProduits.Remove(tJMenuProduit);
            await db.SaveChangesAsync();

            return Ok(tJMenuProduit);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TJMenuProduitExists(int id)
        {
            return db.TJMenuProduits.Count(e => e.MenuID == id) > 0;
        }
    }
}