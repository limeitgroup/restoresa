﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using RestoResa.Model;
using RestoResa.WebApi.Model;

namespace RestoResa.WebApi.Controllers
{
    public class LigneCommandesController : ApiController
    {
        private RestoResaWebApiContext db = new RestoResaWebApiContext();

        // GET: api/LigneCommandes
        public IQueryable<LigneCommande> GetLigneCommandes()
        {
            return db.LigneCommandes;
        }

        // GET: api/LigneCommandes/5
        [ResponseType(typeof(LigneCommande))]
        public async Task<IHttpActionResult> GetLigneCommande(int id)
        {
            LigneCommande ligneCommande = await db.LigneCommandes.FindAsync(id);
            if (ligneCommande == null)
            {
                return NotFound();
            }

            return Ok(ligneCommande);
        }

        // PUT: api/LigneCommandes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutLigneCommande(int id, LigneCommande ligneCommande)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ligneCommande.ID)
            {
                return BadRequest();
            }

            db.Entry(ligneCommande).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LigneCommandeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/LigneCommandes
        [ResponseType(typeof(LigneCommande))]
        public async Task<IHttpActionResult> PostLigneCommande(LigneCommande ligneCommande)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.LigneCommandes.Add(ligneCommande);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = ligneCommande.ID }, ligneCommande);
        }

        // DELETE: api/LigneCommandes/5
        [ResponseType(typeof(LigneCommande))]
        public async Task<IHttpActionResult> DeleteLigneCommande(int id)
        {
            LigneCommande ligneCommande = await db.LigneCommandes.FindAsync(id);
            if (ligneCommande == null)
            {
                return NotFound();
            }

            db.LigneCommandes.Remove(ligneCommande);
            await db.SaveChangesAsync();

            return Ok(ligneCommande);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LigneCommandeExists(int id)
        {
            return db.LigneCommandes.Count(e => e.ID == id) > 0;
        }
    }
}