﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using RestoResa.Model;
using RestoResa.WebApi.Model;

namespace RestoResa.WebApi.Controllers
{
    public class StatutCommandesController : ApiController
    {
        private RestoResaWebApiContext db = new RestoResaWebApiContext();

        // GET: api/StatutCommandes
        public IQueryable<StatutCommande> GetStatutCommandes()
        {
            return db.StatutCommandes;
        }

        // GET: api/StatutCommandes/5
        [ResponseType(typeof(StatutCommande))]
        public async Task<IHttpActionResult> GetStatutCommande(int id)
        {
            StatutCommande statutCommande = await db.StatutCommandes.FindAsync(id);
            if (statutCommande == null)
            {
                return NotFound();
            }

            return Ok(statutCommande);
        }

        // PUT: api/StatutCommandes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutStatutCommande(int id, StatutCommande statutCommande)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != statutCommande.ID)
            {
                return BadRequest();
            }

            db.Entry(statutCommande).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StatutCommandeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/StatutCommandes
        [ResponseType(typeof(StatutCommande))]
        public async Task<IHttpActionResult> PostStatutCommande(StatutCommande statutCommande)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.StatutCommandes.Add(statutCommande);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = statutCommande.ID }, statutCommande);
        }

        // DELETE: api/StatutCommandes/5
        [ResponseType(typeof(StatutCommande))]
        public async Task<IHttpActionResult> DeleteStatutCommande(int id)
        {
            StatutCommande statutCommande = await db.StatutCommandes.FindAsync(id);
            if (statutCommande == null)
            {
                return NotFound();
            }

            db.StatutCommandes.Remove(statutCommande);
            await db.SaveChangesAsync();

            return Ok(statutCommande);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool StatutCommandeExists(int id)
        {
            return db.StatutCommandes.Count(e => e.ID == id) > 0;
        }
    }
}