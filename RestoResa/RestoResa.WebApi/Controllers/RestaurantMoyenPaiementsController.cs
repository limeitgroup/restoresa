﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using RestoResa.Model;
using RestoResa.WebApi.Model;

namespace RestoResa.WebApi.Controllers
{
    public class RestaurantMoyenPaiementsController : ApiController
    {
        private RestoResaWebApiContext db = new RestoResaWebApiContext();

        // GET: api/RestaurantMoyenPaiements
        public IQueryable<RestaurantMoyenPaiement> GetRestaurantMoyenPaiements()
        {
            return db.RestaurantMoyenPaiements;
        }

        // GET: api/RestaurantMoyenPaiements/5
        [ResponseType(typeof(RestaurantMoyenPaiement))]
        public async Task<IHttpActionResult> GetRestaurantMoyenPaiement(int id)
        {
            RestaurantMoyenPaiement RestaurantMoyenPaiement = await db.RestaurantMoyenPaiements.FindAsync(id);
            if (RestaurantMoyenPaiement == null)
            {
                return NotFound();
            }

            return Ok(RestaurantMoyenPaiement);
        }

        // PUT: api/RestaurantMoyenPaiements/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutRestaurantMoyenPaiement(int id, RestaurantMoyenPaiement RestaurantMoyenPaiement)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != RestaurantMoyenPaiement.RestaurantID)
            {
                return BadRequest();
            }

            db.Entry(RestaurantMoyenPaiement).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RestaurantMoyenPaiementExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/RestaurantMoyenPaiements
        [ResponseType(typeof(RestaurantMoyenPaiement))]
        public async Task<IHttpActionResult> PostRestaurantMoyenPaiement(RestaurantMoyenPaiement RestaurantMoyenPaiement)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.RestaurantMoyenPaiements.Add(RestaurantMoyenPaiement);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (RestaurantMoyenPaiementExists(RestaurantMoyenPaiement.RestaurantID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = RestaurantMoyenPaiement.RestaurantID }, RestaurantMoyenPaiement);
        }

        // DELETE: api/RestaurantMoyenPaiements/5
        [ResponseType(typeof(RestaurantMoyenPaiement))]
        public async Task<IHttpActionResult> DeleteRestaurantMoyenPaiement(int id)
        {
            RestaurantMoyenPaiement RestaurantMoyenPaiement = await db.RestaurantMoyenPaiements.FindAsync(id);
            if (RestaurantMoyenPaiement == null)
            {
                return NotFound();
            }

            db.RestaurantMoyenPaiements.Remove(RestaurantMoyenPaiement);
            await db.SaveChangesAsync();

            return Ok(RestaurantMoyenPaiement);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RestaurantMoyenPaiementExists(int id)
        {
            return db.RestaurantMoyenPaiements.Count(e => e.RestaurantID == id) > 0;
        }
    }
}