﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using RestoResa.Model;
using RestoResa.WebApi.Model;

namespace RestoResa.WebApi.Controllers
{
    public class MenuServiceHorairesController : ApiController
    {
        private RestoResaWebApiContext db = new RestoResaWebApiContext();

        // GET: api/MenuServiceHoraires
        public IQueryable<MenuServiceHoraire> GetMenuServiceHoraires()
        {
            return db.MenuServiceHoraires;
        }

        // GET: api/MenuServiceHoraires/5
        [ResponseType(typeof(MenuServiceHoraire))]
        public async Task<IHttpActionResult> GetMenuServiceHoraire(int id)
        {
            MenuServiceHoraire MenuServiceHoraire = await db.MenuServiceHoraires.FindAsync(id);
            if (MenuServiceHoraire == null)
            {
                return NotFound();
            }

            return Ok(MenuServiceHoraire);
        }

        // PUT: api/MenuServiceHoraires/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutMenuServiceHoraire(int id, MenuServiceHoraire MenuServiceHoraire)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != MenuServiceHoraire.MenuID)
            {
                return BadRequest();
            }

            db.Entry(MenuServiceHoraire).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MenuServiceHoraireExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MenuServiceHoraires
        [ResponseType(typeof(MenuServiceHoraire))]
        public async Task<IHttpActionResult> PostMenuServiceHoraire(MenuServiceHoraire MenuServiceHoraire)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MenuServiceHoraires.Add(MenuServiceHoraire);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (MenuServiceHoraireExists(MenuServiceHoraire.MenuID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = MenuServiceHoraire.MenuID }, MenuServiceHoraire);
        }

        // DELETE: api/MenuServiceHoraires/5
        [ResponseType(typeof(MenuServiceHoraire))]
        public async Task<IHttpActionResult> DeleteMenuServiceHoraire(int id)
        {
            MenuServiceHoraire MenuServiceHoraire = await db.MenuServiceHoraires.FindAsync(id);
            if (MenuServiceHoraire == null)
            {
                return NotFound();
            }

            db.MenuServiceHoraires.Remove(MenuServiceHoraire);
            await db.SaveChangesAsync();

            return Ok(MenuServiceHoraire);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MenuServiceHoraireExists(int id)
        {
            return db.MenuServiceHoraires.Count(e => e.MenuID == id) > 0;
        }
    }
}