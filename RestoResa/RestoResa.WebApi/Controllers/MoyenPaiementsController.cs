﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using RestoResa.Model;
using RestoResa.WebApi.Model;

namespace RestoResa.WebApi.Controllers
{
    public class MoyenPaiementsController : ApiController
    {
        private RestoResaWebApiContext db = new RestoResaWebApiContext();

        // GET: api/MoyenPaiements
        public IQueryable<MoyenPaiement> GetMoyenPaiements()
        {
            return db.MoyenPaiements;
        }

        // GET: api/MoyenPaiements/5
        [ResponseType(typeof(MoyenPaiement))]
        public async Task<IHttpActionResult> GetMoyenPaiement(int id)
        {
            MoyenPaiement moyenPaiement = await db.MoyenPaiements.FindAsync(id);
            if (moyenPaiement == null)
            {
                return NotFound();
            }

            return Ok(moyenPaiement);
        }

        // PUT: api/MoyenPaiements/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutMoyenPaiement(int id, MoyenPaiement moyenPaiement)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != moyenPaiement.ID)
            {
                return BadRequest();
            }

            db.Entry(moyenPaiement).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MoyenPaiementExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MoyenPaiements
        [ResponseType(typeof(MoyenPaiement))]
        public async Task<IHttpActionResult> PostMoyenPaiement(MoyenPaiement moyenPaiement)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MoyenPaiements.Add(moyenPaiement);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = moyenPaiement.ID }, moyenPaiement);
        }

        // DELETE: api/MoyenPaiements/5
        [ResponseType(typeof(MoyenPaiement))]
        public async Task<IHttpActionResult> DeleteMoyenPaiement(int id)
        {
            MoyenPaiement moyenPaiement = await db.MoyenPaiements.FindAsync(id);
            if (moyenPaiement == null)
            {
                return NotFound();
            }

            db.MoyenPaiements.Remove(moyenPaiement);
            await db.SaveChangesAsync();

            return Ok(moyenPaiement);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MoyenPaiementExists(int id)
        {
            return db.MoyenPaiements.Count(e => e.ID == id) > 0;
        }
    }
}