﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoResa.Model
{
    [Table("Specialite")]
    public class Specialite
    {
        [Key]
        public int ID { get; set; }

        public string NomSpecialite { get; set; }

        public string DescriptionSpecialite { get; set; }
    }
}
