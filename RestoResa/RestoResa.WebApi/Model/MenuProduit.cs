﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestoResa.Model
{
    public class MenuProduit
    {
        [Key, Column(Order = 0)]
        public int MenuID { get; set; }

        [Key, Column(Order = 1)]
        public int ProduitID { get; set; }

        public virtual Menu Menu { get; set; }

        public virtual Produit Produit { get; set; }
    }
}
