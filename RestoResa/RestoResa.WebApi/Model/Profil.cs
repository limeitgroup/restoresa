﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoResa.Model
{
    [Table("Profil")]
    public class Profil
    {
        public Profil()
        {
            Utilisateurs = new HashSet<User>();
        }

        [Key]
        public int ID { get; set; }

        public string Code { get; set; }

        public string NomProfil { get; set; }

        public virtual ICollection<User> Utilisateurs { get; set; }
    }
}
