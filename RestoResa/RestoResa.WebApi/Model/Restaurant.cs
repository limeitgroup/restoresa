﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace RestoResa.Model
{
    [Table("Restaurant")]
    public class Restaurant
    {
        public Restaurant()
        {
            Personnels = new HashSet<User>();
            ServiceHoraires = new HashSet<ServiceHoraire>();
            RestaurantMoyenPaiement = new HashSet<RestaurantMoyenPaiement>();
            RestaurantSpecialite = new HashSet<RestaurantSpecialite>();
        }

        [Key]
        public int ID { get; set; }

        public string RaisonSociale { get; set; }

        public string Description { get; set; }

        public string Adresse { get; set; }

        public string Ville { get; set; }

        public string CodePostal { get; set; }

        public string Pays { get; set; }

        public string Mail { get; set; }

        public string Telephone { get; set; }

        public virtual ICollection<User> Personnels { get; set; }

        public virtual ICollection<ServiceHoraire> ServiceHoraires { get; set; }
        public virtual ICollection<RestaurantMoyenPaiement> RestaurantMoyenPaiement { get; set; }

        public virtual ICollection<RestaurantSpecialite> RestaurantSpecialite { get; set; }
    }
}
