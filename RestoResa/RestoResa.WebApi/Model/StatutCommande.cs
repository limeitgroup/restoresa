﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoResa.Model
{
    [Table("StatutCommande")]
    public class StatutCommande
    {
        [Key]
        public int ID { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }
    }
}
