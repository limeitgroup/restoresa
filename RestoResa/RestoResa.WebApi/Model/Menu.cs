﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoResa.Model
{
    [Table("Menu")]
    public class Menu
    {
        public Menu()
        {
            MenuServiceHoraire = new HashSet<MenuServiceHoraire>();
            MenuProduit = new HashSet<MenuProduit>();
        }

        [Key]
        public int ID { get; set; }

        public string NomMenu { get; set; }

        public string DescriptionMenu { get; set; }

        public decimal PrixMenu { get; set; }

        public virtual ICollection<MenuServiceHoraire>  MenuServiceHoraire { get; set; }

        public virtual ICollection<MenuProduit>  MenuProduit { get; set; }
    }
}
