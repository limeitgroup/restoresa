﻿using RestoResa.Model;
using System.Data.Entity;
namespace RestoResa.WebApi.Model
{
    public class RestoResaWebApiContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx

        public RestoResaWebApiContext() : base("name=RestoResa")
        {
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Profil> Profils { get; set; }

        public DbSet<Restaurant> Restaurants { get; set; }

        public DbSet<RestaurantSpecialite> RestaurantSpecialites { get; set; }

        public DbSet<Specialite> Specialites { get; set; }

        public DbSet<RestaurantMoyenPaiement> RestaurantMoyenPaiements { get; set; }

        public DbSet<MoyenPaiement> MoyenPaiements { get; set; }

        public DbSet<MenuServiceHoraire> MenuServiceHoraires { get; set; }

        public DbSet<Menu> Menus { get; set; }

        public DbSet<ServiceHoraire> ServiceHoraires { get; set; }

        public DbSet<MenuProduit> MenuProduits { get; set; }

        public DbSet<Produit> Produits { get; set; }

        public DbSet<StatutCommande> StatutCommandes { get; set; }

        public DbSet<LigneCommande> LigneCommandes { get; set; }

        public DbSet<Commande> Commandes { get; set; }
    }
}
