﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace RestoResa.Model
{
    [Table("User")]
    [DataContract(IsReference = true)]
    public class User
    {
        public User()
        {
            Commandes = new HashSet<Commande>();
        }

        [Key]
        public long ID { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Mail { get; set; }
        public int ProfilID { get; set; }
        public int? RestaurantID { get; set; }
        public virtual Profil Profil { get; set; }
        public virtual Restaurant Restaurant { get; set; }
        public virtual ICollection<Commande> Commandes { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
    }
}
