﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestoResa.Model
{
    [Table("ServiceHoraire")]
    public class ServiceHoraire
    {
        public ServiceHoraire()
        {
             MenuServiceHoraire = new HashSet<MenuServiceHoraire>();
            Commandes = new HashSet<Commande>();
        }

        [Key]
        public int ID { get; set; }

        public string DenominationService { get; set; }

        public DateTime HeureDebutService { get; set; }

        public DateTime HeureFinService { get; set; }

        public DateTime DateService { get; set; }

        public int NbPlaceLibres { get; set; }

        public int RestaurantID { get; set; }

        public virtual Restaurant Restaurant { get; set; }

        public virtual ICollection<MenuServiceHoraire> MenuServiceHoraire  { get; set; }

        public virtual ICollection<Commande> Commandes  { get; set; }

    }
}
