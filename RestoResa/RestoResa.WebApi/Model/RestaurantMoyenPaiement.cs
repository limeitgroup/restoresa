﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoResa.Model
{
    public class RestaurantMoyenPaiement
    {
        [Key, Column(Order = 0)]
        public int RestaurantID { get; set; }

        [Key, Column(Order = 1)]
        public int MoyenPaiementID { get; set; }

        public Restaurant Restaurant { get; set; }

        public MoyenPaiement MoyenPaiement { get; set; }
    }
}
