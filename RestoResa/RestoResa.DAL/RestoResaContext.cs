﻿using RestoResa.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoResa.DataAccessLayer
{
    public class RestoResaContext : DbContext 
    {
        static RestoResaContext()
        {
            Database.SetInitializer(new RestoResaInitializer());
        }

        public RestoResaContext()
            : base("RestoResaContext")
        {
            Configuration.AutoDetectChangesEnabled = true;
            Configuration.EnsureTransactionsForFunctionsAndCommands = true;
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
            Configuration.UseDatabaseNullSemantics = false;
            Configuration.ValidateOnSaveEnabled = true;
            Database.Log = WriteConsole;
        }

        /// <summary>
        /// Affiche le code généré par EF dans le console
        /// </summary>
        /// <param name="request"></param>
        private void WriteConsole(string request)
        {
            Console.Write(request);
        }

        public IDbSet<Commande> Commande { get; set; }
        public IDbSet<LigneCommande> LigneCommande { get; set; }
        public IDbSet<Menu> Menu { get; set; }
        public IDbSet<MoyenPaiement> MoyenPaiement { get; set; }
        public IDbSet<Produit> Produit { get; set; }
        public IDbSet<Profil> Profil { get; set; }
        public IDbSet<Restaurant> Restaurant { get; set; }
        public IDbSet<ServiceHoraire> ServiceHoraire { get; set; }
        public IDbSet<Specialite> Specialite { get; set; }
        public IDbSet<StatutCommande> StatutCommande { get; set; }
        public IDbSet<Utilisateur> Utilisateur { get; set; }
        public IDbSet<TJMenuProduit> TJMenuProduit { get; set; }
        public IDbSet<TJMenuServiceHoraire> TJMenuServiceHoraire { get; set; }
        public IDbSet<TJRestaurantMoyenPaiement> TJRestaurantMoyenPaiement { get; set; }
        public IDbSet<TJRestaurantSpecialite> TJRestaurantSpecialite { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

        }
    }
}
