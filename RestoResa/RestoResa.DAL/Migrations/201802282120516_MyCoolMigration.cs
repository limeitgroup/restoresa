namespace RestoResa.DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MyCoolMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Commande",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NbPlaces = c.Int(nullable: false),
                        DateCommande = c.DateTime(nullable: false),
                        UtilisateurID = c.Int(nullable: false),
                        StatutCommandeID = c.Int(nullable: false),
                        ServiceHoraireID = c.Int(nullable: false),
                        Utilisateur_ID = c.Long(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ServiceHoraire", t => t.ServiceHoraireID, cascadeDelete: true)
                .ForeignKey("dbo.Utilisateur", t => t.Utilisateur_ID)
                .ForeignKey("dbo.StatutCommande", t => t.StatutCommandeID, cascadeDelete: true)
                .Index(t => t.StatutCommandeID)
                .Index(t => t.ServiceHoraireID)
                .Index(t => t.Utilisateur_ID);
            
            CreateTable(
                "dbo.LigneCommande",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CommandeID = c.Int(nullable: false),
                        MenuID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Commande", t => t.CommandeID, cascadeDelete: true)
                .ForeignKey("dbo.Menu", t => t.MenuID, cascadeDelete: true)
                .Index(t => t.CommandeID)
                .Index(t => t.MenuID);
            
            CreateTable(
                "dbo.Menu",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NomMenu = c.String(),
                        DescriptionMenu = c.String(),
                        PrixMenu = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TJMenuProduits",
                c => new
                    {
                        MenuID = c.Int(nullable: false),
                        ProduitID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.MenuID, t.ProduitID })
                .ForeignKey("dbo.Menu", t => t.MenuID, cascadeDelete: true)
                .ForeignKey("dbo.Produit", t => t.ProduitID, cascadeDelete: true)
                .Index(t => t.MenuID)
                .Index(t => t.ProduitID);
            
            CreateTable(
                "dbo.Produit",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NomProduit = c.String(),
                        DescriptionProduit = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TJMenuServiceHoraires",
                c => new
                    {
                        MenuID = c.Int(nullable: false),
                        ServiceHoraireID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.MenuID, t.ServiceHoraireID })
                .ForeignKey("dbo.Menu", t => t.MenuID, cascadeDelete: true)
                .ForeignKey("dbo.ServiceHoraire", t => t.ServiceHoraireID, cascadeDelete: true)
                .Index(t => t.MenuID)
                .Index(t => t.ServiceHoraireID);
            
            CreateTable(
                "dbo.ServiceHoraire",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DenominationService = c.String(),
                        HeureDebutService = c.DateTime(nullable: false),
                        HeureFinService = c.DateTime(nullable: false),
                        DateService = c.DateTime(nullable: false),
                        NbPlaceLibres = c.Int(nullable: false),
                        RestaurantID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Restaurant", t => t.RestaurantID, cascadeDelete: true)
                .Index(t => t.RestaurantID);
            
            CreateTable(
                "dbo.Restaurant",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RaisonSociale = c.String(),
                        Description = c.String(),
                        Adresse = c.String(),
                        Ville = c.String(),
                        CodePostal = c.String(),
                        Pays = c.String(),
                        Mail = c.String(),
                        Telephone = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Utilisateur",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        Nom = c.String(),
                        Prenom = c.String(),
                        Mail = c.String(),
                        Password = c.String(),
                        ProfilID = c.Int(nullable: false),
                        RestaurantID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Profil", t => t.ProfilID, cascadeDelete: true)
                .ForeignKey("dbo.Restaurant", t => t.RestaurantID)
                .Index(t => t.ProfilID)
                .Index(t => t.RestaurantID);
            
            CreateTable(
                "dbo.Profil",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        NomProfil = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TJRestaurantMoyenPaiements",
                c => new
                    {
                        RestaurantID = c.Int(nullable: false),
                        MoyenPaiementID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.RestaurantID, t.MoyenPaiementID })
                .ForeignKey("dbo.MoyenPaiement", t => t.MoyenPaiementID, cascadeDelete: true)
                .ForeignKey("dbo.Restaurant", t => t.RestaurantID, cascadeDelete: true)
                .Index(t => t.RestaurantID)
                .Index(t => t.MoyenPaiementID);
            
            CreateTable(
                "dbo.MoyenPaiement",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TypePaiement = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TJRestaurantSpecialites",
                c => new
                    {
                        RestaurantID = c.Int(nullable: false),
                        SpecialiteID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.RestaurantID, t.SpecialiteID })
                .ForeignKey("dbo.Restaurant", t => t.RestaurantID, cascadeDelete: true)
                .ForeignKey("dbo.Specialite", t => t.SpecialiteID, cascadeDelete: true)
                .Index(t => t.RestaurantID)
                .Index(t => t.SpecialiteID);
            
            CreateTable(
                "dbo.Specialite",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NomSpecialite = c.String(),
                        DescriptionSpecialite = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.StatutCommande",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Commande", "StatutCommandeID", "dbo.StatutCommande");
            DropForeignKey("dbo.LigneCommande", "MenuID", "dbo.Menu");
            DropForeignKey("dbo.TJMenuServiceHoraires", "ServiceHoraireID", "dbo.ServiceHoraire");
            DropForeignKey("dbo.TJRestaurantSpecialites", "SpecialiteID", "dbo.Specialite");
            DropForeignKey("dbo.TJRestaurantSpecialites", "RestaurantID", "dbo.Restaurant");
            DropForeignKey("dbo.TJRestaurantMoyenPaiements", "RestaurantID", "dbo.Restaurant");
            DropForeignKey("dbo.TJRestaurantMoyenPaiements", "MoyenPaiementID", "dbo.MoyenPaiement");
            DropForeignKey("dbo.ServiceHoraire", "RestaurantID", "dbo.Restaurant");
            DropForeignKey("dbo.Utilisateur", "RestaurantID", "dbo.Restaurant");
            DropForeignKey("dbo.Utilisateur", "ProfilID", "dbo.Profil");
            DropForeignKey("dbo.Commande", "Utilisateur_ID", "dbo.Utilisateur");
            DropForeignKey("dbo.Commande", "ServiceHoraireID", "dbo.ServiceHoraire");
            DropForeignKey("dbo.TJMenuServiceHoraires", "MenuID", "dbo.Menu");
            DropForeignKey("dbo.TJMenuProduits", "ProduitID", "dbo.Produit");
            DropForeignKey("dbo.TJMenuProduits", "MenuID", "dbo.Menu");
            DropForeignKey("dbo.LigneCommande", "CommandeID", "dbo.Commande");
            DropIndex("dbo.TJRestaurantSpecialites", new[] { "SpecialiteID" });
            DropIndex("dbo.TJRestaurantSpecialites", new[] { "RestaurantID" });
            DropIndex("dbo.TJRestaurantMoyenPaiements", new[] { "MoyenPaiementID" });
            DropIndex("dbo.TJRestaurantMoyenPaiements", new[] { "RestaurantID" });
            DropIndex("dbo.Utilisateur", new[] { "RestaurantID" });
            DropIndex("dbo.Utilisateur", new[] { "ProfilID" });
            DropIndex("dbo.ServiceHoraire", new[] { "RestaurantID" });
            DropIndex("dbo.TJMenuServiceHoraires", new[] { "ServiceHoraireID" });
            DropIndex("dbo.TJMenuServiceHoraires", new[] { "MenuID" });
            DropIndex("dbo.TJMenuProduits", new[] { "ProduitID" });
            DropIndex("dbo.TJMenuProduits", new[] { "MenuID" });
            DropIndex("dbo.LigneCommande", new[] { "MenuID" });
            DropIndex("dbo.LigneCommande", new[] { "CommandeID" });
            DropIndex("dbo.Commande", new[] { "Utilisateur_ID" });
            DropIndex("dbo.Commande", new[] { "ServiceHoraireID" });
            DropIndex("dbo.Commande", new[] { "StatutCommandeID" });
            DropTable("dbo.StatutCommande");
            DropTable("dbo.Specialite");
            DropTable("dbo.TJRestaurantSpecialites");
            DropTable("dbo.MoyenPaiement");
            DropTable("dbo.TJRestaurantMoyenPaiements");
            DropTable("dbo.Profil");
            DropTable("dbo.Utilisateur");
            DropTable("dbo.Restaurant");
            DropTable("dbo.ServiceHoraire");
            DropTable("dbo.TJMenuServiceHoraires");
            DropTable("dbo.Produit");
            DropTable("dbo.TJMenuProduits");
            DropTable("dbo.Menu");
            DropTable("dbo.LigneCommande");
            DropTable("dbo.Commande");
        }
    }
}
